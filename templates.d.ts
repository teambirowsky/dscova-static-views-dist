// Generated by dts-bundle v0.7.3

export const experience: Template<ExpCtx>;
export interface Review {
    id: string;
    createdAt: string;
    title?: string;
    body: string;
    rating: number;
    images: Array<Image>;
    userId: string;
}
export interface Image {
    id: string;
    list: [ImageTransformation];
    url: string;
}
export interface ImageTransformation {
    height: number;
    width: number;
    url: string;
}
export interface Category {
    id: string;
    name: string;
}
export interface City {
    id: string;
    name: string;
    totalExperiences: number;
}
export interface User {
    id: string;
    firstName: string;
    lastName: string;
    avatar?: Image;
    url: string;
}
export const sampleExpCtx: ExpCtx;
export interface ExpCtx {
    id: string;
    createdAt: string;
    reviews: {
        [id: string]: Review;
    };
    bestReviewId: string;
    title: string;
    cover: Image;
    ownerId: string;
    avgRating: number;
    rankInCity: number;
    categories: {
        [id: string]: Category;
    };
    users: {
        [id: string]: User;
    };
    city: City;
    ratingCount: number;
    url: string;
    reviewCount: number;
    dscovaPublisherLogo: string;
    serviceWorkerUrl?: string;
}
export type Template<ctx> = (ctx: ctx) => Promise<string>;

